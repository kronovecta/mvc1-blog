﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inlämning_1___Blog.Models;

namespace Inlämning_1___Blog.ViewModels
{
    public class PostViewModel
    {
        public List<Posts> Posts { get; set; }
        public List<Categories> PostCategories { get; set; }
        public string SearchValue { get; set; }

    public PostViewModel()
        {
            Posts = new List<Posts>();
            PostCategories = new List<Categories>();
        }
    }
}
