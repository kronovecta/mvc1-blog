﻿using Inlämning_1___Blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inlämning_1___Blog.ViewModels
{
    public class SinglePostViewModel
    {
        public Posts Post { get; set; }
        public Categories Category { get; set; }

        public SinglePostViewModel()
        {
            Post = new Posts();
            Category = new Categories();
        }
    }
}
