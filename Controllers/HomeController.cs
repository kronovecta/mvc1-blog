﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Inlämning_1___Blog.Models;
using Inlämning_1___Blog.ViewModels;

namespace Inlämning_1___Blog.Controllers
{
    public class HomeController : Controller
    {
        private BlogContext _context;

        public HomeController(BlogContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            PostViewModel model = new PostViewModel();
            model.Posts = _context.Posts.OrderByDescending(p => p.Postdate).ToList();
            model.PostCategories = _context.Categories.ToList();
            return View(model);
        }

        public IActionResult Search(PostViewModel form)
        {
            if (form.SearchValue != null)
            {
                string searchValue = form.SearchValue.ToString();
                PostViewModel model = new PostViewModel();

                var category = (from cat in _context.Categories where cat.CategoryName.Contains(searchValue) select cat.CategoryId).SingleOrDefault();

                model.Posts = _context.Posts.Where(p => 
                        p.Title.ToLower().Contains(searchValue.ToLower()) || 
                        p.CategoryId.Equals(category)).ToList();

                model.SearchValue = "";

                return View("Index", model);
            } else
            {
                return Redirect("Index");
            }

        }

        public IActionResult ViewCategory(int id)
        {
            PostViewModel model = new PostViewModel();
            model.PostCategories = _context.Categories.ToList();
            model.Posts = _context.Posts.Where(p => p.CategoryId == id).ToList();

            return View("Index", model);
        }

        public IActionResult ViewPost(int id)
        {
            var model = new SinglePostViewModel();
            var post = _context.Posts.SingleOrDefault(p => p.Id == id);

            model.Post = post;
            model.Category = _context.Categories.SingleOrDefault(x => x.CategoryId == post.CategoryId);

            return View(model);
        }

        public IActionResult CreatePost()
        {
            ViewBag.Categories = _context.Categories.ToList();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreatePost(Posts post)
        {
            if (ModelState.IsValid)
            {
                ModelState.Clear();
                _context.Posts.Add(post);
                _context.SaveChanges();
                ViewBag.Categories = _context.Categories.ToList();

                var lastPost = _context.Posts.Last();
                return Redirect("ViewPost/" + lastPost.Id);
            } else
            {
                ViewBag.Error = "There was a problem submitting your post - Try again";
                return View();
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePost(int id)
        {
            var deletePost = (from p in _context.Posts where p.Id == id select p).SingleOrDefault();
            if (ModelState.IsValid)
            {
                _context.Posts.Remove(deletePost);
                _context.SaveChanges();
                ViewBag.FlashMessage = "Post deleted";
                return RedirectToAction("Index", "Home");
            } else
            {
                ViewBag.Error = "Error: Could not delete post";
                return RedirectToAction("Index", "Home");
            }
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
