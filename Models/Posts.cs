﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Inlämning_1___Blog.Models
{
    public partial class Posts
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [StringLength(100, ErrorMessage = "Title can be max 200 characters")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter text to your entry")]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        public DateTime? Postdate { get; set; }
        public DateTime? Editdate { get; set; }
        public int? CategoryId { get; set; }

        [DataType(DataType.MultilineText)]
        public string Summary { get; set; }

        public virtual Categories Category { get; set; }
    }
}
